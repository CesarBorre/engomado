package com.neology.engomado;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.neology.engomado.modelo.DataWS_Engomado;
import com.neology.engomado.utils.ImageUtil;

public class DataWS extends AppCompatActivity {
    DataWS_Engomado dataWS_engomado;

    TextView placa, vin, marca, submarca, modelo, version, clase_tipo, color, uso_vehiculo, propietario;
    ImageView foto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_ws);
        Intent i = getIntent();
        dataWS_engomado = i.getParcelableExtra("dataWS");

        placa = (TextView)findViewById(R.id.placaID);
        placa.setText(dataWS_engomado.getPlaca());
        vin = (TextView)findViewById(R.id.vinID);
        vin.setText(dataWS_engomado.getVin());
        marca = (TextView)findViewById(R.id.marcaID);
        marca.setText(dataWS_engomado.getMarca());
        submarca = (TextView)findViewById(R.id.submarcaID);
        submarca.setText(dataWS_engomado.getSubmarca());
        modelo = (TextView)findViewById(R.id.modeloID);
        modelo.setText(dataWS_engomado.getModelo());
        version = (TextView)findViewById(R.id.versionID);
        version.setText(dataWS_engomado.getVersion());
        clase_tipo = (TextView)findViewById(R.id.clase_tipoID);
        clase_tipo.setText(dataWS_engomado.getClase_tipo());
        color = (TextView)findViewById(R.id.colorID);
        color.setText(dataWS_engomado.getColor());
        uso_vehiculo = (TextView)findViewById(R.id.uso_vehiculoID);
        uso_vehiculo.setText(dataWS_engomado.getUso_vehiculo());
        propietario = (TextView)findViewById(R.id.nombrePropietarioID);
        propietario.setText(dataWS_engomado.getPropietario());
        foto = (ImageView)findViewById(R.id.fotoID);
        ImageUtil.setImg(dataWS_engomado.getFoto(), foto);
    }
}
