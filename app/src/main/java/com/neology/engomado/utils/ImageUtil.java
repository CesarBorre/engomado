package com.neology.engomado.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.widget.ImageView;

/**
 * Created by Cesar Segura Granados on 29/06/2016.
 */

public class ImageUtil {

    public static void setImg(byte[] b, ImageView imageView) {
        byte[] decodedString = Base64.decode(b, Base64.DEFAULT);
        Bitmap bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        imageView.setImageBitmap(bitmap);
    }
}
