package com.neology.engomado;

import android.app.FragmentManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentFilter.MalformedMimeTypeException;
import android.nfc.NfcAdapter;
import android.nfc.tech.NfcF;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.neology.engomado.dialogs.Dialog_Settings;
import com.neology.engomado.subfragments.MainFragment;
import com.neology.engomado.lectura.LecturaTag;
import com.neology.engomado.modelo.DatosChip;
import com.neology.engomado.subfragments.CardFrontFragment;
import com.neology.engomado.subfragments.DataWS_Fragment;
import com.neology.engomado.utils.SnackBar;

public class MainActivity extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener{
    public static final String TAG = MainActivity.class.getSimpleName();

    MainActivity ma;
    NfcAdapter nfcAdapter = null;
    PendingIntent pendingIntent = null;
    IntentFilter[] filters = null;
    String[][] techList = null;
    protected String[] datosTag;

    Intent lecturaIntent;

    TextView texto;
    /**
     * A handler object, used for deferring UI operations.
     */
    private Handler mHandler = new Handler();
    /**
     * Whether or not we're showing the back of the card (otherwise showing the front).
     */
    private boolean mShowingBack = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("VALIDACIÓN DE PLACAS");
        setSupportActionBar(toolbar);

        if (savedInstanceState != null) {
            mShowingBack = (getFragmentManager().getBackStackEntryCount() > 0);
        }

        // Monitor back stack changes
        getFragmentManager().addOnBackStackChangedListener(this);

        // Obtenemos el control sobre el lector de NFC
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        // Si no se encuentra el lector de NFC se cierra aplicacion
        if (nfcAdapter == null) {
            Toast.makeText(this, "error nfc",
                    Toast.LENGTH_LONG).show();
            finish();
            return;

        } else {
            if (!nfcAdapter.isEnabled()) {
                SnackBar.showSnackBar(getResources().getString(R.string.no_nfc), this, 0, R.id.coordinator );
            } else {

                // Creamos un Intent para manejar los datos leidos
                pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                        getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

                // Creamos un filtro de Intent relacionado con descubrir un mensaje NDEF
                IntentFilter ndef = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
                IntentFilter discovery = new IntentFilter(
                        NfcAdapter.ACTION_TAG_DISCOVERED);

                // Configuramos el filtro para que acepte de cualquier tipo de NDEF
                try {
                    ndef.addDataType("*/*");
                } catch (MalformedMimeTypeException e) {
                    throw new RuntimeException("fail", e);
                }
                filters = new IntentFilter[]{ndef, discovery};

                // Configuramos para que lea de cualquier clase de tag NFC
                techList = new String[][]{new String[]{NfcF.class.getName()}};

                Intent lecturaIntent = getIntent();
                Log.d(TAG, "INTENT " + lecturaIntent.toString());
                Log.d(TAG, "PENDING INTENT " + pendingIntent.toString());
                Parcelable[] rawMsgs = lecturaIntent
                        .getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
                Log.d(TAG, "RAW " + rawMsgs);

                if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(lecturaIntent.getAction())) {
                    lecturaNFC(lecturaIntent);
                }
            }
        }


        loadWebView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            showDialog();
        }
        return true;
    }

    public void showDialog() {
        DialogFragment dialogFragment = Dialog_Settings.newInstance();
        dialogFragment.show(getSupportFragmentManager(), "dialog");
    }

    public void ok() {
//        Intent i = getPackageManager().getLaunchIntentForPackage( getPackageName() );
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }

    private void loadWebView() {
        WebView wv = (WebView) findViewById(R.id.webView);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.loadUrl("file:///android_asset/tap.html");
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart()");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart()");
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        Log.d(TAG, "onResume()");
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        Log.d(TAG, "onPause()");
        nfcAdapter.disableForegroundDispatch(this);
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        Log.d(TAG, "onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d(TAG, "onNewIntent()");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        lecturaNFC(intent);
    }

    public void lecturaNFC(Intent intent) {
        try {

            LecturaTag lecturaObj = new LecturaTag(intent);
            datosTag = lecturaObj.lectura();

            DatosChip datosChip;
            if (datosTag != null) {


                Log.d(TAG, "# Datos leidos: " + datosTag.length);
                int i = 0;
                while (i < datosTag.length) {
                    Log.d(TAG, "Dato[" + i + "]: " + datosTag[i]);
                    i++;
                }
                datosChip = new DatosChip(datosTag[0], "CIUDAD DE MÉXICO", " AUTO PARTICULAR");
                //flipCard(datosChip);
                //showFrontCard(datosChip);
                Intent intentFragment = new Intent(getApplicationContext(), MainFragment.class);
                intentFragment.putExtra("datosChip", datosChip);
                startActivity(intentFragment);
            }


        } catch (Exception e) {
            Log.e(TAG, "Excepion::lecturaNFC->" + e.getMessage());
            e.printStackTrace();
        }

    }

    private void showFrontCard(DatosChip datosChip) {
        Bundle args = new Bundle();
        args.putParcelable("datosChip", datosChip);
        CardFrontFragment cardFrontFragment = new CardFrontFragment();
        cardFrontFragment.setArguments(args);
        getFragmentManager()
                .beginTransaction()
                .add(R.id.content_main, cardFrontFragment)
                .commit();
    }

    private void flipCard() {
        if (mShowingBack) {
            getFragmentManager().popBackStack();
            return;
        }
        // Flip to the back.

        mShowingBack = true;

        // Create and commit a new fragment transaction that adds the fragment for the back of
        // the card, uses custom animations, and is part of the fragment manager's back stack.

        getFragmentManager()
                .beginTransaction()

                // Replace the default fragment animations with animator resources representing
                // rotations when switching to the back of the card, as well as animator
                // resources representing rotations when flipping back to the front (e.g. when
                // the system Back button is pressed).
                .setCustomAnimations(
                        R.animator.card_flip_right_in, R.animator.card_flip_right_out,
                        R.animator.card_flip_left_in, R.animator.card_flip_left_out)

                // Replace any fragments currently in the container view with a fragment
                // representing the next page (indicated by the just-incremented currentPage
                // variable).
                .replace(R.id.content_main, new DataWS_Fragment())

                // Add this transaction to the back stack, allowing users to press Back
                // to get to the front of the card.
                .addToBackStack(null)

                // Commit the transaction.
                .commit();

        // Defer an invalidation of the options menu (on modern devices, the action bar). This
        // can't be done immediately because the transaction may not yet be committed. Commits
        // are asynchronous in that they are posted to the main thread's message loop.
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                invalidateOptionsMenu();
            }
        });
    }

    @Override
    public void onBackStackChanged() {
        mShowingBack = (getFragmentManager().getBackStackEntryCount() > 0);

        // When the back stack changes, invalidate the options menu (action bar).
        invalidateOptionsMenu();
    }
}