package com.neology.engomado.subfragments;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.neology.engomado.MainActivity;
import com.neology.engomado.R;
import com.neology.engomado.VolleyApp;
import com.neology.engomado.modelo.DataWS_Engomado;
import com.neology.engomado.modelo.DatosChip;
import com.neology.engomado.subfragments.CardFrontFragment;
import com.neology.engomado.subfragments.DataWS_Fragment;
import com.neology.engomado.utils.CheckInternetConnection;
import com.neology.engomado.utils.SnackBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class MainFragment extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener{

    /**
     * A handler object, used for deferring UI operations.
     */
    private Handler mHandler = new Handler();

    /**
     * Whether or not we're showing the back of the card (otherwise showing the front).
     */
    private boolean mShowingBack = false;

    DatosChip datosChip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_fragment);
        Intent intent = getIntent();
        datosChip = intent.getParcelableExtra("datosChip");

        Bundle args = new Bundle();
        args.putParcelable("datosChip", datosChip);
        CardFrontFragment cardFrontFragment = new CardFrontFragment();
        cardFrontFragment.setArguments(args);

        if (savedInstanceState == null) {
            // If there is no saved instance state, add a fragment representing the
            // front of the card to this activity. If there is saved instance state,
            // this fragment will have already been added to the activity.
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.activity_main_fragment, cardFrontFragment)
                    .commit();
        } else {
            mShowingBack = (getFragmentManager().getBackStackEntryCount() > 0);
        }

        // Monitor back stack changes to ensure the action bar shows the appropriate
        // button (either "photo" or "info").
        getFragmentManager().addOnBackStackChangedListener(this);
    }

    public void setDataFrontFragment(final Activity a, TextView placa, TextView entidad, TextView tipoServicio, ImageView auto,
                                     FloatingActionButton searchBtn,
                                     final int id) {
        placa.setText(datosChip.getPlaca());
        if (datosChip.getPlaca().equals("A01-AAA")) {
            auto.setImageResource(R.drawable.dodge_neon);
        } else if (datosChip.getPlaca().equals("LMN-23-45")) {
            auto.setImageResource(R.drawable.charger);
        } else if (datosChip.getPlaca().equals("999-ZZZ")) {
            auto.setImageResource(R.drawable.seat);
        }
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(CheckInternetConnection.isConnectedToInternet(getApplicationContext())) {
                    callWs(datosChip.getPlaca());
                } else {
                    Toast.makeText(getApplicationContext(), "Activar WIFI", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void callWs(String placa) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                "http://mobile.neology-demos.com:8080/api/engomado?placa="+placa,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("CardFrontManager", response.toString());
                        readJson(response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CardFrontManager", error.toString());
//                        getString(R.string.errorHost)+" "+sharedPreferences.getString(Constants_Settings.KEY_URL, null)
                        Toast.makeText(getApplicationContext(),"Error", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return super.getParams();
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                //return Auth();
                return super.getHeaders();
            }

        };
        VolleyApp.getmInstance().addToRequestQueue(jsonObjectRequest);
    }

    private void readJson(String response) {
        new jsonEngomado().execute(response);
    }

    class jsonEngomado extends AsyncTask<String, Void, Boolean> {
        DataWS_Engomado dataWS_engomado;

        @Override
        protected Boolean doInBackground(String... strings) {
            try {
                JSONObject jsonObject = new JSONObject(strings[0]);
                JSONObject engomado = jsonObject.getJSONObject("engomados");
                dataWS_engomado = new DataWS_Engomado(
                        engomado.getString("placa"),
                        engomado.getString("vin"),
                        engomado.getString("marca"),
                        engomado.getString("submarca"),
                        engomado.getString("modelo"),
                        engomado.getString("version"),
                        engomado.getString("clase_tipo"),
                        engomado.getString("color"),
                        engomado.getString("uso_vehiculo"),
                        engomado.getString("propietario"),
                        engomado.getString("foto").getBytes());

            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {

                Bundle args = new Bundle();
                args.putParcelable("dataWS", dataWS_engomado);
                args.putParcelable("dataChip", datosChip);
                DataWS_Fragment fragment = new DataWS_Fragment();
                fragment.setArguments(args);
                flipCard(fragment);
            }
        }
    }

    private void flipCard(DataWS_Fragment fragment) {
        if (mShowingBack) {
            getFragmentManager().popBackStack();
            return;
        }

        // Flip to the back.

        mShowingBack = true;

        // Create and commit a new fragment transaction that adds the fragment for the back of
        // the card, uses custom animations, and is part of the fragment manager's back stack.

        getFragmentManager()
                .beginTransaction()

                // Replace the default fragment animations with animator resources representing
                // rotations when switching to the back of the card, as well as animator
                // resources representing rotations when flipping back to the front (e.g. when
                // the system Back button is pressed).
                .setCustomAnimations(
                        R.animator.card_flip_right_in, R.animator.card_flip_right_out,
                        R.animator.card_flip_left_in, R.animator.card_flip_left_out)

                // Replace any fragments currently in the container view with a fragment
                // representing the next page (indicated by the just-incremented currentPage
                // variable).
                .replace(R.id.activity_main_fragment, fragment)

                // Add this transaction to the back stack, allowing users to press Back
                // to get to the front of the card.
                .addToBackStack(null)

                // Commit the transaction.
                .commit();

        // Defer an invalidation of the options menu (on modern devices, the action bar). This
        // can't be done immediately because the transaction may not yet be committed. Commits
        // are asynchronous in that they are posted to the main thread's message loop.
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                invalidateOptionsMenu();
            }
        });
    }

    @Override
    public void onBackStackChanged() {
        mShowingBack = (getFragmentManager().getBackStackEntryCount() > 0);

        // When the back stack changes, invalidate the options menu (action bar).
        invalidateOptionsMenu();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(this.getClass().getSimpleName(), "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(this.getClass().getSimpleName(), "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(this.getClass().getSimpleName(), "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(this.getClass().getSimpleName(), "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(this.getClass().getSimpleName(), "onDestroy");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(this.getClass().getSimpleName(), "onRestart");
    }

}
