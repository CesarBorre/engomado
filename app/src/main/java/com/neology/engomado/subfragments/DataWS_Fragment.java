package com.neology.engomado.subfragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.neology.engomado.R;
import com.neology.engomado.modelo.DataWS_Engomado;
import com.neology.engomado.modelo.DatosChip;
import com.neology.engomado.utils.ImageUtil;

/**
 * Created by LeviAcosta on 23/08/2016.
 */

public class DataWS_Fragment extends Fragment {

    DataWS_Engomado dataWS_engomado;
    DatosChip datosChip;
    TextView placa, vin, marca, submarca, modelo, version, clase_tipo, color, uso_vehiculo, propietario;
    ImageView foto;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataWS_engomado = getArguments().getParcelable("dataWS");
        datosChip = getArguments().getParcelable("dataChip");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_data_ws, container, false);
        placa = (TextView)v.findViewById(R.id.placaID);
        placa.setText(dataWS_engomado.getPlaca());
        vin = (TextView)v.findViewById(R.id.vinID);
        vin.setText(dataWS_engomado.getVin());
        marca = (TextView)v.findViewById(R.id.marcaID);
        marca.setText(dataWS_engomado.getMarca());
        submarca = (TextView)v.findViewById(R.id.submarcaID);
        submarca.setText(dataWS_engomado.getSubmarca());
        modelo = (TextView)v.findViewById(R.id.modeloID);
        modelo.setText(dataWS_engomado.getModelo());
        version = (TextView)v.findViewById(R.id.versionID);
        version.setText(dataWS_engomado.getVersion());
        clase_tipo = (TextView)v.findViewById(R.id.clase_tipoID);
        clase_tipo.setText(dataWS_engomado.getClase_tipo());
        color = (TextView)v.findViewById(R.id.colorID);
        color.setText(dataWS_engomado.getColor());
        uso_vehiculo = (TextView)v.findViewById(R.id.uso_vehiculoID);
        uso_vehiculo.setText(dataWS_engomado.getUso_vehiculo());
        propietario = (TextView)v.findViewById(R.id.nombrePropietarioID);
        propietario.setText(dataWS_engomado.getPropietario());
        foto = (ImageView)v.findViewById(R.id.fotoID);
        ImageUtil.setImg(dataWS_engomado.getFoto(), foto);
        return v;
    }

}
