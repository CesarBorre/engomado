package com.neology.engomado.modelo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by LeviAcosta on 22/08/2016.
 */

public class DatosChip implements Parcelable{

    private String placa;
    private String entidad;
    private String tipoAuto;

    public DatosChip (String placa, String entidad, String tipoAuto) {
        this.placa = placa;
        this.entidad = entidad;
        this.tipoAuto = tipoAuto;
    }

    protected DatosChip(Parcel in) {
        placa = in.readString();
        entidad = in.readString();
        tipoAuto = in.readString();
    }

    public static final Creator<DatosChip> CREATOR = new Creator<DatosChip>() {
        @Override
        public DatosChip createFromParcel(Parcel in) {
            return new DatosChip(in);
        }

        @Override
        public DatosChip[] newArray(int size) {
            return new DatosChip[size];
        }
    };

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getTipoAuto() {
        return tipoAuto;
    }

    public void setTipoAuto(String tipoAuto) {
        this.tipoAuto = tipoAuto;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(placa);
        parcel.writeString(entidad);
        parcel.writeString(tipoAuto);
    }
}
