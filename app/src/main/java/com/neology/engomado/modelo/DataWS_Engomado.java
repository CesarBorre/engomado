package com.neology.engomado.modelo;

import android.os.Parcel;
import android.os.Parcelable;
import android.provider.ContactsContract;

/**
 * Created by LeviAcosta on 23/08/2016.
 */

public class DataWS_Engomado implements Parcelable{

    private String placa;
    private String vin;
    private String marca;
    private String submarca;
    private String modelo;
    private String version;
    private String clase_tipo;
    private String color;
    private String uso_vehiculo;
    private String propietario;
    private byte [] foto;

    public DataWS_Engomado(String placa,
                           String vin,
                           String marca,
                           String submarca,
                           String modelo,
                           String version,
                           String clase_tipo,
                           String color,
                           String uso_vehiculo,
                           String propietario,
                           byte[] foto) {
        this.placa = placa;
        this.vin = vin;
        this.marca = marca;
        this.submarca = submarca;
        this.modelo = modelo;
        this.version = version;
        this.clase_tipo = clase_tipo;
        this.color = color;
        this.uso_vehiculo = uso_vehiculo;
        this.propietario = propietario;
        this.foto = foto;
    }

    protected DataWS_Engomado(Parcel in) {
        placa = in.readString();
        vin = in.readString();
        marca = in.readString();
        submarca = in.readString();
        modelo = in.readString();
        version = in.readString();
        clase_tipo = in.readString();
        color = in.readString();
        uso_vehiculo = in.readString();
        propietario = in.readString();
        foto = in.createByteArray();
    }

    public static final Creator<DataWS_Engomado> CREATOR = new Creator<DataWS_Engomado>() {
        @Override
        public DataWS_Engomado createFromParcel(Parcel in) {
            return new DataWS_Engomado(in);
        }

        @Override
        public DataWS_Engomado[] newArray(int size) {
            return new DataWS_Engomado[size];
        }
    };

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getSubmarca() {
        return submarca;
    }

    public void setSubmarca(String submarca) {
        this.submarca = submarca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getClase_tipo() {
        return clase_tipo;
    }

    public void setClase_tipo(String clase_tipo) {
        this.clase_tipo = clase_tipo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getUso_vehiculo() {
        return uso_vehiculo;
    }

    public void setUso_vehiculo(String uso_vehiculo) {
        this.uso_vehiculo = uso_vehiculo;
    }

    public String getPropietario() {
        return propietario;
    }

    public void setPropietario(String propietario) {
        this.propietario = propietario;
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(placa);
        parcel.writeString(vin);
        parcel.writeString(marca);
        parcel.writeString(submarca);
        parcel.writeString(modelo);
        parcel.writeString(version);
        parcel.writeString(clase_tipo);
        parcel.writeString(color);
        parcel.writeString(uso_vehiculo);
        parcel.writeString(propietario);
        parcel.writeByteArray(foto);
    }
}
